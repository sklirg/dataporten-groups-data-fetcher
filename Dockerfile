FROM python:3.6-alpine

VOLUME ["/srv/app/dumps"]
EXPOSE 5000

WORKDIR /srv/app

RUN apk add --update --no-cache \
    build-base \
    linux-headers \
    gcc \
    musl-dev \
    python3-dev \
    libffi-dev \
    openssl-dev

COPY requirements requirements
RUN pip install \
    -r requirements/base.txt \
    -r requirements/prod.txt

COPY . .

CMD ["uwsgi", "uwsgi.ini"]
