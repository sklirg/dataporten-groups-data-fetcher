import json
import logging
import os
from datetime import datetime
from urllib.parse import urlencode
from uuid import uuid4

import arrow
import requests
from flask import Flask, redirect, request, session
from oic import rndstr
from oic.oauth2 import AuthorizationResponse
from oic.oic import Client, RegistrationResponse
from oic.utils.authn.client import CLIENT_AUTHN_METHOD

from study import *

DATAPORTEN_PROVIDER_CONFIG = 'https://auth.dataporten.no/'
DATAPORTEN_CLIENT_ID = os.getenv('CLIENT_ID')
DATAPORTEN_CLIENT_SECRET = os.getenv('CLIENT_SECRET')
DATAPORTEN_REDIRECT_URI = os.getenv('REDIRECT_URI')
DATAPORTEN_SCOPES = ['openid', 'userid', 'profile', 'groups', 'email']

logger = logging.getLogger(__name__)


app = Flask(__name__)
app.secret_key = os.getenv('SECRET_KEY')


def client_setup(client_id, client_secret):
    """Sets up an OpenID Connect Relying Party ("client") for connecting to Dataporten"""

    logger = logging.getLogger(__name__)

    assert client_id, 'Missing client id when setting up Dataporten OpenID Connect Relying Party'
    assert client_secret, 'Missing client secret when setting up Dataporten OpenID Connect Relying Party'

    client = Client(client_authn_method=CLIENT_AUTHN_METHOD)

    logger.debug('Automatically registering Dataporten OpenID Provider.', extra={'config': DATAPORTEN_PROVIDER_CONFIG})
    client.provider_config(DATAPORTEN_PROVIDER_CONFIG)
    client_args = {
        'client_id': client_id,
        'client_secret': client_secret,
    }
    client.store_registration_info(RegistrationResponse(**client_args))
    logger.debug('Successfully registered the provider.')

    return client


@app.route('/')
def study():
    client = client_setup(DATAPORTEN_CLIENT_ID, DATAPORTEN_CLIENT_SECRET)
    user = str(uuid4())

    # Set user session identifier
    session['user'] = user

    # Generate random values used to verify that it's the same user when in the callback.
    state = rndstr()
    nonce = rndstr()

    session['dataporten_study_state'] = state
    session['dataporten_study_nonce'] = nonce

    args = {
        'client_id': DATAPORTEN_CLIENT_ID,
        'response_type': 'code',
        'scope': DATAPORTEN_SCOPES,
        'redirect_uri': DATAPORTEN_REDIRECT_URI,
        'nonce': nonce,
        'state': state,
    }

    logger.debug(
        'Constructing authorization request and redirecting user to authorize through Dataporten.',
        extra={'user': user}
    )

    auth_req = client.construct_AuthorizationRequest(request_args=args)
    login_url = auth_req.request(client.authorization_endpoint)

    return redirect(login_url)


@app.route('/callback')
def study_callback():
    user = session.get('user', 'none')
    logger.debug('Fetching study programme for user {}'.format(user), extra={'user': user})

    client = client_setup(DATAPORTEN_CLIENT_ID, DATAPORTEN_CLIENT_SECRET)

    queryparams = str(request.query_string.decode(encoding='UTF-8'))
    print('qp', queryparams)

    auth_resp = client.parse_response(AuthorizationResponse, info=queryparams, sformat='urlencoded')

    if not session.get('dataporten_study_state', '') or \
            session['dataporten_study_state'] != auth_resp['state']:
        logger.info('Dataporten state did not equal the one in session!')
        return redirect('/err')

    args = {
        'code': auth_resp['code'],
        'redirect_uri': DATAPORTEN_REDIRECT_URI,
    }

    token_request = client.do_access_token_request(
        state=auth_resp['state'], request_args=args, authn_method='client_secret_basic',
    )

    access_token = token_request.get('access_token')

    # Do user info request
    userinfo = client.do_user_info_request(state=auth_resp['state'], behavior='use_authorization_header')
    ntnu_username_dataporten = userinfo.get('email').split('@')[0]

    # Getting information about study of the user
    groups = fetch_groups_information(access_token)

    os.makedirs('dumps', exist_ok=True)

    now = arrow.now()
    fname = f'dumps/{ntnu_username_dataporten}_{now.isoformat()}.json'
    with open(fname, 'w') as f:
        f.write(json.dumps(groups))

    session['last_dump'] = fname

    return redirect('/ty')


@app.route('/ty')
def thanks():
    if session.get('last_dump'):
        fname = session.get('last_dump')
        with open(fname, 'r') as f:
            groups = json.load(f)
        study_group = get_study(groups)
        study_id = get_group_id(study_group)
        study_year = get_year(study_id, groups)
        study_name = study_group.get('displayName')
        field_of_study = get_field_of_study(groups)

        # Remove the years from bachelor if the user is a master student.
        if study_year >= 4:
            start_date_for_study = study_year - 3
        else:
            start_date_for_study = study_year

        started_date = datetime(arrow.now().year - start_date_for_study, 7, 1)

        return f'Thanks for your data! Is it correct that you study {study_name} on year {study_year}? If not, please contact hakon@solbj.org or sklirg.'
    return 'Thanks!'


@app.route('/err')
def err():
    return 'Det ser ut som noe gikk galt. Vennligst prøv igjen.'


def fetch_groups_information(access_token, show_all=False):
    logger.debug('Fetching groups info...')
    query_params = urlencode({
        'show_all': show_all
    })
    groups_api = 'https://groups-api.dataporten.no/groups/me/groups?%s' % query_params
    groups_resp = requests.request('GET', groups_api, headers={'Authorization': 'Bearer ' + access_token})

    return json.loads(groups_resp.content.decode(encoding='UTF-8'))


if __name__ == '__main__':
    app.run()
